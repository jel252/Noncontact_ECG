## Noncontact_ECG

We built a non-contact electrocardiograph (ECG) sensor with Arduino and our customized non-contact electrodes to remove motion artifacts. For the schematics, please refer to our publication[1].
The repository contains Arduino firmwares, a windows app for the receiving of ECG data through bluetooth, and a matlab code for the analysis, post-filtering and our algorithm to remove motion artifacts.
In the following figure, we successfully recover the raw ECG to the waveform of wet-contact ECG signal, which is our baseline.
<br><img src="https://gitlab.com/jel252/Noncontact_ECG/raw/master/Img/Recovered.png"  width="50%" ><br>

As long as you cite this reposity and our Non-contact ECG paper on BioCAS2015, you are free to use my codes for any
non-commerical purpose.

## Hardware

The device must be a portable device powered by batteries so that unnecessary noises can be isolated from our experiments.
The hardware device is composed with three parts: (1) an Arduino micro-controller, (2) a RC05 bluetooth module, (3) Electrodes.
The following pictures shows our customized non-contact electrodes.
<br><img src="https://gitlab.com/jel252/Noncontact_ECG/raw/master/Img/Electrodes.png"  width="50%" ><br>

The schematic of electrodes is shown in the following figure.
<br><img src="https://gitlab.com/jel252/Noncontact_ECG/raw/master/Img/Schematics.png"  width="50%" ><br>
It is a 4-way design embedded in two electrodes.

## Arduino Firmware

The micro-controller on Arduino needs to control the internal ADCs and the RC05 BLE module, and pass the output data of ADCs through the soft-UART channels to computers.
The Highkey Arduino firmware is to initialize a RC05 module when the first time to set up registers of RC05.

## Windows Application

The windows app is written with C#. For the soft-UART controller I directly use the code snippet on a website, which is already down. I would really like to credit on the author of the soft-UART part, but I do not know how to.
The Windows app allows you to set the sampling rate and and log the ECG signals into csv files, and also display the raw data waveforms like a oscilloscope.

## MATLAB post-processing

We use MATLAB to implement our algorithm for the post-processing.
<br><img src="https://gitlab.com/jel252/Noncontact_ECG/raw/master/Img/Algorithm.png"  width="50%" ><br>
For the detailed explanation, please see our publication[1].

The improvements are listed in the following table.
<br><img src="https://gitlab.com/jel252/Noncontact_ECG/raw/master/Img/Tab_I.png"  width="50%" ><br>


## Reference

[1] Lin, Jeng-Hau, Hao Liu, Chia-Hung Liu, Phillip Lam, Gung-Yu Pan, Hao Zhuang, Ilgweon Kang, Patrick P. Mercier, and Chung-Kuan Cheng. "An interdigitated non-contact ECG electrode for impedance compensation and signal restoration." In Biomedical Circuits and Systems Conference (BioCAS), 2015 IEEE, pp. 1-4. IEEE, 2015.
