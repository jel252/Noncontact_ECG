% 2015-05-25
% bioCAS_1_system: test multiple bandpass filter
% By Jeng-Hau Lin
clc; clear all; close all;
set(0,'DefaultFigureColor',[1 1 1]);
% file = ('./ECG_signals/2015-05-23/M_sit_01.csv'); %v
file = ('./ECG_signals/M_sit_03.csv'); %v
% file = ('./ECG_signals/2015-05-23/M_sit_04.csv'); %v
% file = ('./ECG_signals/2015-05-23/M_sit_05.csv'); %v
% file = ('./ECG_signals/2015-05-23/M_walk_05.csv'); %v
% file = ('./ECG_signals/2015-05-23/M_walk_06.csv'); %v
% file = ('./ECG_signals/2015-05-23/M_walk_12.csv'); %v

% file = ('./ECG_signals/2015-05-20/Mike_walk_10.csv'); %v
[pathstr,name,ext] = fileparts(file);
N = 10000;
A = csvread(file);
t = A(1:N,1)';
dt = t(2)-t(1);
% N = length(t);
xp1 = -A(1:N,2)';
xp2 = -A(1:N,3)';
xp3 = -A(1:N,4)'*1.8;




% x1= sin(10*2*pi*t)+sin(2*2*pi*t);
df = 1/(dt*N);
f = -df*ceil((N-1)/2):df:df*ceil((N-1)/2-1);
fmax = f(end);
fse = fmax;


f1 = fft(xp1)/N; % This is the difference of continuous and discrete
f1 = [f1(ceil(N/2)+1:end) f1(1:ceil(N/2))];
f1p = [f1(ceil(N/2)+1:end) f1(1:ceil(N/2))];
ix1 = ifft(f1p)*N; % This is the difference of continuous and discrete


c60 = fir1(1000,(58)/fse,'low'); % Use fir1
xq1 = filter(c60, 1 , xp1);
xq2 = filter(c60, 1 , xp2);
xq3 = filter(c60, 1 , xp3);


c = fir1(1000,(1)/fse,'low'); % Use fir1
baseline1 = filter(c, 1 , xp1);
baseline2 = filter(c, 1 , xp2);
baseline3 = filter(c, 1 , xp3);

x1 = xq1 - baseline1;
x2 = xq2 - baseline2;
x3 = xq3 - baseline3;
% Finite impulse response filters split the ECG into 5 component

% flc = 9;
% fhc = 11;
% c = fir1(1000,([flc fhc])/fse,'stop'); % Use fir1
bp1 = fir1(1000,([0.5       1.5646])/fse,   'bandpass'); % Use fir1
bp2 = fir1(1000,([1.5646    4.8957])/fse,   'bandpass'); % Use fir1
bp3 = fir1(1000,([4.8957    15.3194])/fse,  'bandpass'); % Use fir1
bp4 = fir1(1000,([15.3194   47.9366])/fse,  'bandpass'); % Use fir1
bp5 = fir1(1000,([47.9366   85])/fse,      'bandpass'); % Use fir1

xf1_1 = filter(bp1, 1 , x1);
xf1_2 = filter(bp2, 1 , x1);
xf1_3 = filter(bp3, 1 , x1);
xf1_4 = filter(bp4, 1 , x1);
xf1_5 = filter(bp5, 1 , x1);

env1_1 = abs(hilbert(xf1_1));
env1_2 = abs(hilbert(xf1_2));
env1_3 = abs(hilbert(xf1_3));
env1_4 = abs(hilbert(xf1_4));
env1_5 = abs(hilbert(xf1_5));

xf2_1 = filter(bp1, 1 , x2);
xf2_2 = filter(bp2, 1 , x2);
xf2_3 = filter(bp3, 1 , x2);
xf2_4 = filter(bp4, 1 , x2);
xf2_5 = filter(bp5, 1 , x2);

env2_1 = abs(hilbert(xf2_1));
env2_2 = abs(hilbert(xf2_2));
env2_3 = abs(hilbert(xf2_3));
env2_4 = abs(hilbert(xf2_4));
env2_5 = abs(hilbert(xf2_5));

xf3_1 = filter(bp1, 1 , x3);
xf3_2 = filter(bp2, 1 , x3);
xf3_3 = filter(bp3, 1 , x3);
xf3_4 = filter(bp4, 1 , x3);
xf3_5 = filter(bp5, 1 , x3);
% env3_1 = abs(hilbert(xf3_1));
% env3_2 = abs(hilbert(xf3_2));
% env3_3 = abs(hilbert(xf3_3));
% env3_4 = abs(hilbert(xf3_4));
% env3_5 = abs(hilbert(xf3_5));
xo1 = xf1_1+xf1_2+xf1_3+xf1_4+xf1_5;
xo2 = xf2_1+xf2_2+xf2_3+xf2_4+xf2_5;
xo3 = xf3_1+xf3_2+xf3_3+xf3_4+xf3_5;

figure(1)
subplot(3,2,1)
plot(t,xf1_1, t,xf2_1, t, env1_1,'r--', t, env2_1,'k--');
subplot(3,2,2)
plot(t,xf1_2, t,xf2_2, t, env1_2,'r--', t, env2_2,'k--');
subplot(3,2,3)
plot(t,xf1_3, t,xf2_3, t, env1_3,'r--', t, env2_3,'k--');
subplot(3,2,4)
plot(t,xf1_4, t,xf2_4, t, env1_4,'r--', t, env2_4,'k--');
subplot(3,2,5)
plot(t,xf1_5, t,xf2_5, t, env1_5,'r--', t, env2_5,'k--');
subplot(3,2,6)
plot(t,xf1_5, t,xf2_5, t, env1_5,'r--', t, env2_5,'k--');

figure(2)
subplot(3,1,1)
% plot(t,x1,t,ix1);%xf1);
% plot(t,x1,t,xf1_1+xf1_2+xf1_3+xf1_4+xf1_5);
% plot(t,x1, t,x2);
plot(t,xo1,...
     t, xo3);
%      t
subplot(3,1,2)
plot(f,abs(f1));

subplot(3,1,3)
plot(f,angle(f1));





% % % % % % % % % % % % % % % % % % % % Recovery
s = j*2*pi*[1 2 5 16 48];
Rd = 30e9 * ones(1, length(s));
Cino = 65e-12;;
Cp1 = (0 )*1e-12+Cino;
Cp2 = (100)*1e-12+Cino;
Zcp1 = 1./s/Cp1;
Zcp2 = 1./s/Cp2;
Zin1 = para(Rd, Zcp1);
Zin2 = para(Rd, Zcp2);
% Ze = zeros(1, length(s));
Ze_1 = zeros(1, N);
Ze_2 = zeros(1, N);
Ze_3 = zeros(1, N);
Ze_4 = zeros(1, N);
Ze_5 = zeros(1, N);

ECG1_1 = zeros(1, N);
ECG1_2 = zeros(1, N);
ECG1_3 = zeros(1, N);
ECG1_4 = zeros(1, N);
ECG1_5 = zeros(1, N);

ECG2_1 = zeros(1, N);
ECG2_2 = zeros(1, N);
ECG2_3 = zeros(1, N);
ECG2_4 = zeros(1, N);
ECG2_5 = zeros(1, N);

% % Give up envelop
% env1_1 = xf1_1;
% env1_2 = xf1_2;
% env1_3 = xf1_3;
% env1_4 = xf1_4;
% env1_5 = xf1_5;
% env2_1 = xf2_1;
% env2_2 = xf2_2;
% env2_3 = xf2_3;
% env2_4 = xf2_4;
% env2_5 = xf2_5;

threshold = 4e6;
% threshold = 5e6;
% threshold = 8e6;

for i = 1:N
    if(abs(Zin1(1)*env2_1(i) - Zin2(1)*env1_1(i)) < threshold)
        Ze_1(i) = 0;
    else
        Ze_1(i) = Zin2(1)*Zin1(1)*(env1_1(i)-env2_1(i))/...
                  (Zin1(1)*env2_1(i) - Zin2(1)*env1_1(i));
    end
    
    ECG1_1(i) = xf1_1(i)/abs(Zin1(1)/(Ze_1(i)+Zin1(1)));
    ECG2_1(i) = xf2_1(i)/abs(Zin2(1)/(Ze_1(i)+Zin2(1)));

    if(abs(Zin1(2)*env2_2(i) - Zin2(2)*env1_2(i)) < threshold)
        Ze_2(i) = 0;
    else
        Ze_2(i) = Zin2(2)*Zin1(2)*(env1_2(i)-env2_2(i))/...
                  (Zin1(2)*env2_2(i) - Zin2(2)*env1_2(i));
    end
    
    ECG1_2(i) = xf1_2(i)/abs(Zin1(2)/(Ze_2(i)+Zin1(2)));
    ECG2_2(i) = xf2_2(i)/abs(Zin2(2)/(Ze_2(i)+Zin2(2)));

    if(abs(Zin1(3)*env2_3(i) - Zin2(3)*env1_3(i)) < threshold)
        Ze_3(i) = 0;
    else
        Ze_3(i) = Zin2(3)*Zin1(3)*(env1_3(i)-env2_3(i))/...
                  (Zin1(3)*env2_3(i) - Zin2(3)*env1_3(i));
    end
    
    ECG1_3(i) = xf1_3(i)/abs(Zin1(3)/(Ze_3(i)+Zin1(3)));
    ECG2_3(i) = xf2_3(i)/abs(Zin2(3)/(Ze_3(i)+Zin2(3)));

    if(abs(Zin1(4)*env2_4(i) - Zin2(4)*env1_4(i)) < threshold)
        Ze_4(i) = 0;
    else
        Ze_4(i) = Zin2(4)*Zin1(4)*(env1_4(i)-env2_4(i))/...
                  (Zin1(4)*env2_4(i) - Zin2(4)*env1_4(i));
    end
    
    ECG1_4(i) = xf1_4(i)/abs(Zin1(4)/(Ze_4(i)+Zin1(4)));
    ECG2_4(i) = xf2_4(i)/abs(Zin2(4)/(Ze_4(i)+Zin2(4)));
    
    if(abs(Zin1(5)*env2_5(i) - Zin2(5)*env1_5(i)) < threshold)
        Ze_5(i) = 0;
    else
        Ze_5(i) = Zin2(5)*Zin1(5)*(env1_5(i)-env2_5(i))/...
                  (Zin1(5)*env2_5(i) - Zin2(5)*env1_5(i));
    end
    
    ECG1_5(i) = xf1_5(i)/abs(Zin1(5)/(Ze_5(i)+Zin1(5)));
    ECG2_5(i) = xf2_5(i)/abs(Zin2(5)/(Ze_5(i)+Zin2(5)));

end

a1 = ECG1_1 + ECG1_2 + ECG1_3 + ECG1_4 + ECG1_5;
a2 = ECG2_1 + ECG2_2 + ECG2_3 + ECG2_4 + ECG2_5;


figure(3)
subplot(2,1,1)
plot(t, xo1, t, a1, t, xo3, 'r--');
legend('original Ch.1', 'Recovered Ch.1', 'Wet');
xlim([12 16]);
subplot(2,1,2)
plot(t, xo2, t, a2, t, xo3, 'r--');
legend('original Ch.2', 'Recovered Ch.2', 'Wet');
xlim([12 16]);




% % % % % % % % % % % % % Calculate improvement

% avoide = 1500;
avoids = find(t>4.5,1);
avoide = find(t>7.5,1);

y1 = xo1(avoids:avoide);
y2 = a1(avoids:avoide);
y3 = xo3(avoids:avoide);
y1n=y1/norm(y1);
y2n=y2/norm(y2);
y3n=y3/norm(y3);
normratio13=norm(y1)/norm(y3);
normratio23=norm(y2)/norm(y3);
% c13=conv2(y1n,y3n,'same');
% c23=conv2(y2n,y3n,'same');
c13 = y1n.*y3n;
c23 = y2n.*y3n;
[val13 ind13]=max(c13);
[val23 ind23]=max(c23);
sumC13= sum(c13)
sumC23 = sum(c23)

theta13 = acos(sumC13);
theta23 = acos(sumC23);



diss13 = 1-sumC13;
diss23 = 1-sumC23;

% improve = -(theta23-theta13)/theta13*100
% improve = (sumC23 - sumC13)/sumC13 *100
improve = -(diss23-diss13)/diss13*100


figure(4)
subplot(3,1,1)
plot(t,abs(Ze_1+Ze_2+Ze_3+Ze_4+Ze_5))
subplot(3,1,2)
plot(c13)
subplot(3,1,3)
plot(c23)

d = fir1(1000,(40)/fse,'low'); % Use fir1
af1 = filter(d, 1 , a1);
xof1 = filter(d, 1 , xo2);
xof3 = filter(d, 1 , xo3);

tshift = 14;
figure(5)
plot(t-tshift, xof1,'-.',t-tshift,af1,  t+2-tshift,x3,'r--','Linewidth',1.5)
% xlim([14 19.2]);
% xlim([10 14]);
xlim([0 3]);
ylim([-0.8 0.6]);

xlabel('Time (s)')
ylabel('Voltage (V)');
title('Recovered ECG');
legend('Raw data', 'Recovered',  'Wet-contact')
%# make all text in the figure to size 14 and bold
set(gca,'FontSize',20);
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',20,'fontWeight','bold')

return

figure(6)
plot(t+2,xq1, t+2, x1, t+2, baseline1, 'r--','Linewidth',2)
% xlim([17.92 19.2]);
xlim([10 14]);
ylim([-3 3])
xlabel('time (s)')
ylabel('voltage (V)');
title('Detrend');
legend('Raw Data', 'Detrended')
%# make all text in the figure to size 14 and bold
set(gca,'FontSize',20);
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',20,'fontWeight','bold')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use envelop to avoid crossing zero errors
% Use peak detector to get the envelop of the two channels
MPD = 130;
[~,locs_Rwave1] = findpeaks(x1,'MinPeakHeight',0.1,'MinPeakDistance',MPD);
x1_inverted = -x1;
[~,locs_Swave1] = findpeaks(x1_inverted,'MinPeakHeight',0.1,'MinPeakDistance',MPD);
vp1 = interp1(t(locs_Rwave1),x1(locs_Rwave1),t,'spline');
vn1 = interp1(t(locs_Swave1),x1(locs_Swave1),t,'spline');
vp1 = [ones(1,locs_Rwave1(1)-1)*vp1(locs_Rwave1(1)) ...
    vp1(locs_Rwave1(1):locs_Rwave1(end)) ...
ones(1,length(vp1)-locs_Rwave1(end))*vp1(locs_Rwave1(end))];
vn1 = [ones(1,locs_Swave1(1)-1)*vn1(locs_Swave1(1)) ...
    vn1(locs_Swave1(1):locs_Swave1(end)) ...
ones(1,length(vn1)-locs_Swave1(end))*vn1(locs_Swave1(end))];

[~,locs_Rwave2] = findpeaks(af1,'MinPeakHeight',0.1,'MinPeakDistance',MPD);                                
af1_inverted = -af1;
[~,locs_Swave2] = findpeaks(af1_inverted,'MinPeakHeight',0.1,'MinPeakDistance',MPD);
vp2 = interp1(t(locs_Rwave2),af1(locs_Rwave2),t,'spline');
vn2 = interp1(t(locs_Swave2),af1(locs_Swave2),t,'spline');
vp2 = [ones(1,locs_Rwave2(1)-1)*vp2(locs_Rwave2(1)) ...
    vp2(locs_Rwave2(1):locs_Rwave2(end)) ...
ones(1,length(vp2)-locs_Rwave2(end))*vp2(locs_Rwave2(end))];
vn2 = [ones(1,locs_Swave2(1)-1)*vn2(locs_Swave2(1)) ...
    vn2(locs_Swave2(1):locs_Swave2(end)) ...
ones(1,length(vn2)-locs_Swave2(end))*vn2(locs_Swave2(end))];


amp1 = vp1 - vn1;
amp2 = vp2 - vn2;

figure(7)
subplot(3,1,1)
    hold on;
    box on;
    plot(t+2, x1);
    plot(t(locs_Rwave1)+2,x1(locs_Rwave1),'rv','MarkerFaceColor','r');
    plot(t(locs_Swave1)+2,x1(locs_Swave1),'rs','MarkerFaceColor','b');
    plot(t+2, vp1, t+2, vn1);
    xlim([1 20]);
%     ylim([-0.5 0.5]);
%     title('Envelop of Ch.1');
subplot(3,1,2)
    hold on;
    box on;
    plot(t, af1);
    plot(t(locs_Rwave2),af1(locs_Rwave2),'rv','MarkerFaceColor','r');
    plot(t(locs_Swave2),af1(locs_Swave2),'rs','MarkerFaceColor','b');
    plot(t, vp2, t, vn2);
    xlim([1 20]);
subplot(3,1,3)
    hold on;
    plot(t+2, vp1,'b', t+2, vn1,'b');
    plot(t, vp2,'r', t, vn2,'r');
    xlim([1 20]);

%# make all text in the figure to size 14 and bold
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',20,'fontWeight','bold')


return

figure(1)
loglog(Re, Gl, Re, z1, Re, p1);
legend('G_l', 'z_1', 'p_1')
% xlim([0.01, 2]*1e9);
xlabel('R_e (\Omega)');
% ylabel('')
set(gca,'FontSize',20);
%# make all text in the figure to size 14 and bold
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',20,'fontWeight','bold')


f = logspace(-2,4,1001); 
w = 2*pi*f;
s = j*w;
Gh = Cc/(Cc+Cp);
Re = 2e9;
z1 = 1./(Cc*Re);
p1 = (Rd+Re)./(Rd*Re*(Cc+Cp));
H = Gh*(s+z1)./(s+p1);
% angH = atan(-2*pi*f*(Re*Cc-Rd*Cp)*Re/(Re+Rd));
% angH = atan(w/z1)-atan(w/p1);
% angH = atan(w*(z1-p1)./(-w.^2-p1*z1));
% angH2 = (w*(Re*Cp)/(Re*Cc*Re*(Cc+Cp))./(-w.^2-1/(Re*Cc*Re*(Cc+Cp))));

figure(2)
% plot(f, angle(H),f, angH, f, angH2)
semilogx(f,20*log10(abs(H)));
% legend('G_l', 'z_1', 'p_1')
% ylim([-pi/2 0]);
ylim([-6 0]);
% xlim([0.01, 2]*1e9);
% xlabel('R_e (\Omega)');
% ylabel('')
set(gca,'FontSize',20);
%# make all text in the figure to size 14 and bold
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',20,'fontWeight','bold')