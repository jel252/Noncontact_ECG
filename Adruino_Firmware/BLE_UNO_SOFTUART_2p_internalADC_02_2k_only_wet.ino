// 2014-10-30
// Test TimerOne
// By Jeng-Hau Lin

// Defines 
//Baud rate 
#define BaudRate 115200
#define SampRate 2000
#define PINKEY 7
#define PINBTRX 5
#define PINBTTX 6

// Includes
#include <SoftwareSerial.h>
#include <TimerOne.h>

SoftwareSerial mySerial(5, 6); // UNO RX=D5, TX=D6

int ch1;
int ch2;
int ch3;
//byte AnalogHex[12] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

// Timer1 interrupt service routine
void timerIsr()
{
  ch1 = analogRead(0);
//  ch2 = analogRead(1);
//  ch3 = analogRead(2);
  
  mySerial.write(byte(253));
  mySerial.write(byte(255));
  mySerial.write(highByte(ch1));
  mySerial.write(lowByte(ch1));
//  mySerial.write(byte(0));
//  mySerial.write(byte(0));
//  mySerial.write(highByte(ch2));
//  mySerial.write(lowByte(ch2));
//  mySerial.write(byte(0));
//  mySerial.write(byte(0));
//  mySerial.write(highByte(ch3));
//  mySerial.write(lowByte(ch3));
}

void setup() 
{
  Serial.begin(BaudRate);
  while (!Serial) ;	 // For Leonardo and my UNO, must wait!
  Serial.println("Non-contact Sensor.");

  mySerial.begin(BaudRate);
  delay(50);            // Wait until SoftwareSerial is ready.
}

void loop() 
{
  if(Serial.available())
    mySerial.write(Serial.read());

  if(mySerial.available())
  {
    byte tmpChar = mySerial.read();
    if(tmpChar == 'a')
    {
      Timer1.initialize(1000000/SampRate);
      Timer1.attachInterrupt(timerIsr);

    }
    else if (tmpChar == 's')
    {
      Timer1.detachInterrupt();
    }
    else
      Serial.write((byte)tmpChar);
  }
}
