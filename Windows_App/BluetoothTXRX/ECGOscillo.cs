﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace BluetoothTXRX
{
    public delegate void LogEventHandler(object sender, EventArgs e);           // Set a delegate named SomeEventHandler 

    public partial class ECGOscillo : Form
    {
        public List<int> xList = new List<int>();
        //public List<double> yList1 = new List<double>();
        //public List<double> yList2 = new List<double>();
        public List<double> yList3 = new List<double>();

        public event LogEventHandler LogEvent;                                  // Create a public delegate event method
        public ECGOscillo()
        {
            InitializeComponent();
        }

        private void ECGOscillo_FormClosing(object sender, FormClosingEventArgs e)
        {
            LogEvent(sender, e);
            this.Hide();
            e.Cancel = true; // this cancels the close event.
        }

        private void ECGOscillo_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                //ECGCht.Series["Ch. 1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                //ECGCht.Series["Ch. 2"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                //ECGOscillo_timer1.Start();
                //chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);
            }
            else
            {
                //ECGOscillo_timer1.Stop();
            }
        }

        public void ResetExceptionState(Control control)
        {
            typeof(Control).InvokeMember("SetState", BindingFlags.NonPublic |
              BindingFlags.InvokeMethod | BindingFlags.Instance, null,
              control, new object[] { 0x400000, false });
        }

        private void ECGCht_Click(object sender, EventArgs e)
        {

        }
    }
}
