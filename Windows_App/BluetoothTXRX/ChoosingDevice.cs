﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BluetoothTXRX
{
    public delegate void DialEventHandler(DialEventArgs e); 

    public partial class ChoosingDevice : Form
    {
        public event DialEventHandler DialEvent;                                   // While the if condition occurs, call this event
        
        // Record discovered bluetooth low energy device, add it into this list
        public static List<string> DiscoveredDevicesList = new List<string>();
        
        public ChoosingDevice()
        {
            InitializeComponent();
            //this.DialEvent += new DialEventHandler(ChoosingDevice_DialEvent); // Monit preferenceBox1's closing event
        }


        private void ChoosingDevice_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void DialBtn_Click(object sender, EventArgs e)
        {
            if (AddrescBx.Text != "")
            {
                DialEventArgs DEArg = new DialEventArgs(AddrescBx.Text);
                DialEvent(DEArg);
                this.Hide();
            }
        }

        private void ChoosingDevice_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                AddrescBx.Items.Clear();
                foreach (string S in DiscoveredDevicesList)
                {
                    AddrescBx.Items.Add(S);
                }
                //AddrescBx.DataSource = DiscoveredDevicesList;
            }
        }
    }

    public class DialEventArgs : EventArgs
    {
        public DialEventArgs(string Address)
        {
            Data = Address;
        }

        /// <summary>
        /// Byte array containing data from serial port
        /// </summary>
        public string Data;
    }
}
