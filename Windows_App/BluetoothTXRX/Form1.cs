﻿#region Namespace Inclusions
using BluetoothTXRX.Properties;
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
#endregion

namespace BluetoothTXRX
{
    #region Public Enumerations
    public enum LogMsgType { Incoming, Outgoing, Normal, Warning, Error };
    #endregion

    public partial class Form1 : Form
    {
        #region Local Variables

        // The main control for communicating through the RS-232 port
        private SerialPort comport = new SerialPort();

        // Various colors for logging info
        private Color[] LogMsgTypeColor = { Color.Blue, Color.Green, Color.Black, Color.Orange, Color.Red };

        // Temp holder for whether a key was pressed
        private bool KeyHandled = false;

        private Settings settings = Settings.Default;

        // Record whether a command is "ATDILE".
        private bool IsDiscover = false;

        // Instance of Choose bluetooth device
        private ChoosingDevice myChoosingDevice = new ChoosingDevice();
        public ECGOscillo myECGOscillo = new ECGOscillo();
        // 
        private bool IsEndData = true;
        private bool IsAcquire = false;
        private bool IsSaveClicked = false;
        private bool IsSaving = false;
        private int SavingCounter = 0;
        private bool prevIsAcq = false;
        public double Ch1Value = 3.3;

        public List<Byte> Bytes = new List<Byte>();
        public List<string> LogString = new List<string>();

        
        public int xtime = 1;
        public int xtime_window = 2000;  // Display 20 s
        public static string tempLog = Application.StartupPath.ToString() + "\\TempLog.txt";                                      // Temp log file full name

        public List<Byte> tempList = new List<Byte>();
        public bool is0x0D = false;

        public List<int> tmpxList = new List<int>();
        public volatile List<double> tmpyList1 = new List<double>();
        public volatile List<double> tmpyList2 = new List<double>();
        public volatile List<double> tmpyList3 = new List<double>();
        public List<byte> tmpRemainedBytes = new List<byte>();
        #endregion

        public Form1()
        {
            // Load user settings
            settings.Reload();

            // Build the form
            InitializeComponent();

            // Restore the users settings
            InitializeControlValues();

            // Enable/disable controls based on the current state
            EnableControls();

            // When data is recieved through the port, call this method
            comport.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            //comport.PinChanged += new SerialPinChangedEventHandler(comport_PinChanged);
            myChoosingDevice.DialEvent += new DialEventHandler(myChoosingDevice_DialEvent);
           
            myECGOscillo.LogEvent += new LogEventHandler(ECGOscillo_LogEvent); // Monit preferenceBox1's closing event
                     
            //tmpxList = new List<int>(xtime_window);
            //tmpyList = new List<double>(xtime_window);
        }

        #region Local Methods

        private void myChoosingDevice_DialEvent(DialEventArgs de)
        {
            IsDiscover = false;
            ChoosingDevice.DiscoveredDevicesList.Clear();
            SendData("ATDMLE," + de.Data);
        }

        /// <summary> Save the user's settings. </summary>
        private void SaveSettings()
        {
            settings.BaudRate = int.Parse(BaRatecBx.Text);
            settings.DataBits = int.Parse(DatBitcBx.Text);
            settings.Parity = (Parity)Enum.Parse(typeof(Parity), ParitycBx.Text);
            settings.StopBits = (StopBits)Enum.Parse(typeof(StopBits), StoBitcBx.Text);
            settings.PortName = PoNamecBx.Text;

            settings.Save();
        }

        /// <summary> Populate the form's controls with default settings. </summary>
        private void InitializeControlValues()
        {
            ParitycBx.Items.Clear(); ParitycBx.Items.AddRange(Enum.GetNames(typeof(Parity)));
            StoBitcBx.Items.Clear(); StoBitcBx.Items.AddRange(Enum.GetNames(typeof(StopBits)));

            StoBitcBx.Text = settings.StopBits.ToString();
            DatBitcBx.Text = settings.DataBits.ToString();
            ParitycBx.Text = settings.Parity.ToString();
            BaRatecBx.Text = settings.BaudRate.ToString();

            RefreshComPortList();

            // If it is still avalible, select the last com port used
            if (PoNamecBx.Items.Contains(settings.PortName)) PoNamecBx.Text = settings.PortName;
            else if (PoNamecBx.Items.Count > 0) PoNamecBx.SelectedIndex = PoNamecBx.Items.Count - 1;
            else
            {
                MessageBox.Show(this, "There are no COM Ports detected on this computer.\nPlease install a COM Port and restart this app.", "No COM Ports Installed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        /// <summary> Enable/disable controls based on the app's current state. </summary>
        private void EnableControls()
        {
            // Enable/disable controls based on whether the port is open or not
            PortSegBx.Enabled = !comport.IsOpen;
            ATcmdsgBx.Enabled = comport.IsOpen;
            SendDCgBx.Enabled = comport.IsOpen;
            //chkDTR.Enabled = chkRTS.Enabled = comport.IsOpen;

            if (comport.IsOpen) OpenPoBtn.Text = "&Close Port";
            else OpenPoBtn.Text = "&Open Port";
        }

        /// <summary> Send the user's data currently entered in the 'send' box.</summary>
        private void SendData(string DatCmd)
        {
            DatCmd += "\r\n";
            // Send the user's text straight out the port
            comport.Write(DatCmd);   // It is very inportant to add an "\r" here.

            // Show in the terminal window the user's text
            Log(true, DatCmd);

            DatCmdtBx.SelectAll();
        }

        private bool ParseComma(string S1)
        {
            if (S1.Length < 5) return false;
            bool ParResult = false;
            int numberComma = 0;

            char[] S1Arr = S1.ToArray();
            List<char> tmpS = new List<char>();
            foreach (char ch in S1Arr)
            {
                if (ch == ',')
                {
                    numberComma++;
                    try
                    {
                        string tmpToS = new string(tmpS.ToArray());
                        Byte deci1 = System.Convert.ToByte(tmpToS);
                        Bytes.Add(deci1);
                        tmpS.Clear();
                    }
                    catch { }
                }
                else if (ch == '\n')
                {
                    try
                    {
                        string tmpToS = new string(tmpS.ToArray());
                        Byte deci1 = System.Convert.ToByte(tmpToS);
                        Bytes.Add(deci1);
                        tmpS.Clear();
                    }
                    catch { }
                }
                else
                {
                    tmpS.Add(ch);
                }
            }

            if (numberComma == 2)
                ParResult = true;
            return ParResult;
        }

        /// <summary> Log data to the terminal window. </summary>
        /// <param name="io"> true for send-outs and false for received. </param>
        /// <param name="msg"> The string containing the message to be shown. </param>
        private void Log(bool io,string msg)
        {
            if (msg.Length > 25 && !IsAcquire)
            {
                /*
                 * string subS = ((string)LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[1].Value).Substring(0, 12);
                if (subS == "DISCOVERY,2,")
                {
                    string tempS = ((string)LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[1].Value).Substring(12, 12);
                    if (!ChoosingDevice.DiscoveredDevicesList.Contains(tempS))
                    {
                        ChoosingDevice.DiscoveredDevicesList.Add(tempS);
                    }
                }
                 */
                comport.DiscardInBuffer();
                return;
            }

            LogHisDGV.BeginInvoke(new EventHandler(delegate
            {
                if (LogHisDGV.Rows.Count >= 300)
            {
                LogHisDGV.Rows.Clear();
            }

            if (IsEndData && !IsAcquire)
            {
                LogHisDGV.Rows.Add(1);
                IsEndData = false;
            }

            if (LogHisDGV.RowCount > 0 && !IsAcquire)
            {
                if (io)
                {
                    LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[0].Style = new DataGridViewCellStyle { ForeColor = Color.Blue };
                    LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[1].Style = new DataGridViewCellStyle { ForeColor = Color.Blue };
                }
                else
                {
                    LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[0].Style = new DataGridViewCellStyle { ForeColor = Color.Green };
                    LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[1].Style = new DataGridViewCellStyle { ForeColor = Color.Green };
                }
                LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[0].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                LogHisDGV.Rows[LogHisDGV.RowCount - 1].Cells[1].Value += msg;
                LogHisDGV.FirstDisplayedScrollingRowIndex = LogHisDGV.RowCount - 1;
            }
            }));



            if ((msg.Length > 0) && ((msg.ToArray())[msg.Length - 1] == '\n'))
            {
                IsEndData = true;
            }


        }

        #endregion

        #region Event Handlers

        private void Form1_Shown(object sender, EventArgs e)
        {
            Log(true, "Application Started \r\n");
        }

        private void frmTerminal_FormClosing(object sender, FormClosingEventArgs e)
        {
            // The form is closing, save the user's preferences
            SaveSettings();
        }

        private void BaRatecBx_Validating(object sender, CancelEventArgs e)
        { int x; e.Cancel = !int.TryParse(BaRatecBx.Text, out x); }

        private void DatBitcBx_Validating(object sender, CancelEventArgs e)
        { int x; e.Cancel = !int.TryParse(DatBitcBx.Text, out x); }

        private void OpenPoBtn_Click(object sender, EventArgs e)
        {
            bool error = false;

            // If the port is open, close it.
            if (comport.IsOpen) comport.Close();
            else
            {
                // Set the port's settings
                comport.BaudRate = int.Parse(BaRatecBx.Text);
                comport.DataBits = int.Parse(DatBitcBx.Text);
                comport.StopBits = (StopBits)Enum.Parse(typeof(StopBits), StoBitcBx.Text);
                comport.Parity = (Parity)Enum.Parse(typeof(Parity), ParitycBx.Text);
                comport.PortName = PoNamecBx.Text;
                comport.RtsEnable = true;

                try
                {
                    // Open the port
                    comport.Open();
                }
                catch (UnauthorizedAccessException) { error = true; }
                catch (IOException) { error = true; }
                catch (ArgumentException) { error = true; }

                if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Change the state of the form's controls
            EnableControls();

            // If the port is open, send focus to the send data box
            if (comport.IsOpen)
            {
                DatCmdtBx.Focus();
                //if (chkClearOnOpen.Checked) ClearTerminal();
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            prevIsAcq = false;
            SendData(DatCmdtBx.Text); 
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // If the com port has been closed, do nothing
            if (!comport.IsOpen) return;

            // This method will be called when there is data waiting in the port's buffer

            if (prevIsAcq == false && IsAcquire == false)
            {
                // Read all the data waiting in the buffer
                string data = comport.ReadExisting();
                //Console.WriteLine(data);

                List<string> PunchedStrings = PunchingString(data);

                for (int Sindex = 0; Sindex != PunchedStrings.Count; ++Sindex)
                {
                    
                    // Display the text to the user in the terminal
                    Log(false, PunchedStrings[Sindex]);
                }
            }
            else if (prevIsAcq == true && IsAcquire == false)
            {
                
                //string line = String.Empty;
                try
                {
                    for(int i=0;i!=10;++i)
                    comport.DiscardInBuffer();
                    //line = comport.ReadLine();
                    //line = line.Trim();

                }
                catch
                { }
            }
            else // If the ECG require command is sent...
            {

                //uint tmpInt1;
                //uint tmpInt2;
                //Int32 receivedValue = comport.ReadByte();
                //tempList.Add((Byte)receivedValue);
                byte[] tmpReceivedBytes = new byte[comport.BytesToRead];
                comport.Read(tmpReceivedBytes, 0, tmpReceivedBytes.Length);
                foreach (byte abyte in tmpReceivedBytes)
                {
                    tmpRemainedBytes.Add(abyte);
                }


                /*
                while (tmpRemainedBytes.Count>4 &&tmpRemainedBytes[4] != 0x00)
                {
                    //tmpRemainedBytes.RemoveRange(0,1);
                    tmpRemainedBytes.Insert(0, 0x00);
                }
                */
                List<byte> newBytes = new List<byte>();

                while (tmpRemainedBytes.Count >= 6)
                {
                    var index0xFD = tmpRemainedBytes.FindIndex(a => a == 0xFD);
                    // FD FF XX XX FD FF
                    //  0  1  2  3  4  5
                    if (index0xFD == 0 && tmpRemainedBytes[index0xFD+1] == 0xFF)
                    {
                        tmpRemainedBytes.RemoveRange(0, 2);

                        //var index0xFD2 = tmpRemainedBytes.FindIndex(a => a == 0xFD);
                        //var index0x002 = tmpRemainedBytes.FindIndex(a => a == 0x00);
                        newBytes.Add(0x00);
                        newBytes.Add(0x00);
                        // XX XX FD FF
                        //  0  1  2  3
                        if (tmpRemainedBytes[2] == 0xFD && tmpRemainedBytes[3] == 0xFF)
                        {
                            newBytes.AddRange(tmpRemainedBytes.GetRange(0, 2));
                            tmpRemainedBytes.RemoveRange(0, 2);
                        }
                        else
                        {
                            var index0xFD2 = tmpRemainedBytes.FindIndex(a => a == 0xFD);
                            if (index0xFD2 != -1)
                            {
                                // XX XX ...
                                //  0  1 
                                for (int index0 = 0; index0 != 2; ++index0)
                                {
                                    newBytes.Add(0x00);
                                }
                                tmpRemainedBytes.RemoveRange(0, 2);
                            }
                            else
                            {
                                if (tmpRemainedBytes[index0xFD + 1] == 0xFF)
                                {
                                    // XX FD FF
                                    //  0  1  2
                                    for (int index0 = 0; index0 != 2; ++index0)
                                    {
                                        newBytes.Add(0x00);
                                    }
                                    tmpRemainedBytes.RemoveRange(0, index0xFD2);
                                }
                                else
                                {
                                    for (int index0 = 0; index0 != 2; ++index0)
                                    {
                                        newBytes.Add(0x00);
                                    }
                                    tmpRemainedBytes.RemoveRange(0, 2);
                                }

                             }
                        }
                    }
                    else
                    {
                        
                        for (int index0 = 0; index0 != 4; ++index0)
                        {
                            newBytes.Add(0x00);
                        }
                        //tmpRemainedBytes.RemoveRange(0, 12);
                         
                        if (index0xFD != -1 && tmpRemainedBytes[index0xFD + 1] == 0xFF)
                        {
                            tmpRemainedBytes.RemoveRange(0, index0xFD);
                        }
                        else 
                        {
                            tmpRemainedBytes.RemoveRange(0, 4);
                        }
                    }

                    //List<byte> newBytes = tmpRemainedBytes.GetRange(0, 12);
                    //tmpRemainedBytes.RemoveRange(0, 12);

                    newBytes.Reverse();
                    byte[] testArr = newBytes.ToArray();

                    //Console.WriteLine(testArr[3]);
                    //Console.WriteLine(testArr[7]);
                    //Int32 tmpInt1 = BitConverter.ToInt32(testArr, 8);
                    //Int32 tmpInt2 = BitConverter.ToInt32(testArr, 4);
                    Int32 tmpInt3 = BitConverter.ToInt32(testArr, 0);
                    //Console.WriteLine(tmpInt1);
                    //Console.WriteLine(tmpInt2);
                    //Console.WriteLine(tmpInt3);
                    //Double tmpDouble1 = 5*  (((double)tmpInt1 - 511.0) / 1024.0);
                    //Double tmpDouble2 = 5 * (((double)tmpInt2 - 511.0) / 1024.0);
                    Double tmpDouble3 = 5 * (((double)tmpInt3 - 511.0) / 1024.0);
                    

                    // Add the latest data point
                    //myECGOscillo.yList1.Add(tmpDouble1);
                    //myECGOscillo.yList2.Add(tmpDouble2);
                    myECGOscillo.yList3.Add(tmpDouble3);

                    if (IsSaving)
                    {
                        //tmpyList1.Add(tmpDouble1);
                        //tmpyList2.Add(tmpDouble2);
                        tmpyList3.Add(tmpDouble3);
                    }

                    if (xtime <= xtime_window)
                    {
                        myECGOscillo.xList.Add(xtime);
                    }

                    if (xtime > xtime_window)
                    {
                        //myECGOscillo.yList1.RemoveAt(0);
                        //myECGOscillo.yList2.RemoveAt(0);
                        myECGOscillo.yList3.RemoveAt(0);
                    }

                    if (xtime % 1000 == 0)
                    {
                        /*
                        myECGOscillo.xList = tmpxList;
                        myECGOscillo.yList1 = tmpyList1;
                        myECGOscillo.yList2 = tmpyList2;
                        */
                        if (IsSaveClicked)
                        {
                            IsSaving = true;
                            IsSaveClicked = false;
                        }
                        if (IsSaving)
                        {
                            ++SavingCounter;
                            if (SavingCounter == 12)
                            {
                                //tmpyList1.Clear();
                                //tmpyList2.Clear();
                                tmpyList3.Clear();
                                SavingCounter = 0;
                                IsSaving = false;
                            }
                        }
                        
                        myECGOscillo.ECGCht.Invoke(new EventHandler(delegate
                        {
                            //myECGOscillo.ECGCht.Series["Ch. 1"].Points.Clear();
                            //myECGOscillo.ECGCht.Series["Ch. 1"].Points.DataBindXY(myECGOscillo.xList, myECGOscillo.yList1);
                            //myECGOscillo.ECGCht.Series["Ch. 2"].Points.Clear();
                            //myECGOscillo.ECGCht.Series["Ch. 2"].Points.DataBindXY(myECGOscillo.xList, myECGOscillo.yList2);
                            myECGOscillo.ECGCht.Series["Cont"].Points.DataBindXY(myECGOscillo.xList, myECGOscillo.yList3);

                            //myECGOscillo.ResetExceptionState(myECGOscillo.ECGCht);
                        }));
                        myECGOscillo.ECGCht.Invoke(new EventHandler(delegate
                        {
                            myECGOscillo.ECGCht.Invalidate();
                            if (IsSaving == true && SavingCounter == 11)
                            {
                                IsAcquire = false;
                                SendData("stop_acq");
                                try
                                {
                                    // Call the built-in default saveFileDialog box
                                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)                // If confirmed the file to be saved as
                                    {
                                        //Console.WriteLine(tmpyList1.Count);
                                        //Console.WriteLine(tmpyList2.Count);
                                        //Console.WriteLine(tmpyList3.Count);
                                        var fInfo = new FileInfo(saveFileDialog1.FileName);

                                        FileStream fs = fInfo.Create();                                        // Create the new real file
                                        StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);           // Create a over-write stream writer

                                        string txy;
                                        //int upperlimit = (xtime_window < tmpxList.Count)?(xtime_window):(tmpxList.Count);
                                        for (int tindex = 0; tindex != tmpyList3.Count; ++tindex)
                                        {
                                            //txy = 0.002 * tindex + "," + tmpyList1[tindex] + "," + tmpyList2[tindex] + ", " + tmpyList3[tindex];
                                            txy = 0.0005 * tindex + "," + tmpyList3[tindex];// +"," + tmpyList2[tindex] + ", " + tmpyList3[tindex];
                                            sw.WriteLine(txy);
                                        }

                                        sw.Close();                                                         // Close the stream writer
                                    }
                                    // Else, if choose not to save file as, do nothing
                                }
                                catch
                                {
                                }
                            }
                        }));
                    }

                    xtime++;
                }
            }
        }

        private List<string> PunchingString(string OriS)
        {
            List<string> PunchedList = new List<string>();

            if (OriS.Contains("\n") || OriS.Contains("\r"))
            {
                char[] TempCharArray = new char[OriS.Length];
                TempCharArray = OriS.ToArray();
                string tempString = "";

                for (int i = 0; i < TempCharArray.Length; i++ )
                {
                    tempString += TempCharArray[i];

                    if ((TempCharArray[i] == '\n') && (tempString != "\r\n"))
                    {
                        PunchedList.Add(tempString);
                        tempString = "";
                    }
                    else if (tempString == "\r\n")
                    {
                        tempString = "";
                    }
                    else if (i == TempCharArray.Length - 1)
                    {
                        PunchedList.Add(tempString);
                        tempString = "";
                    }
                }
            }
            else
            {
                PunchedList.Add(OriS);
            }

            return PunchedList;
        }

        private void SendDCBtn_KeyDown(object sender, KeyEventArgs e)
        {
            // If the user presses [ENTER], send the data now
            if (KeyHandled = e.KeyCode == Keys.Enter)
            {
                prevIsAcq = false;
                e.Handled = true; SendData(DatCmdtBx.Text);
            }
        }

        private void SendDCBtn_KeyPress(object sender, KeyPressEventArgs e)
        { e.Handled = KeyHandled; }
        #endregion

        private void tmrCheckComPorts_Tick(object sender, EventArgs e)
        {
            // checks to see if COM ports have been added or removed
            // since it is quite common now with USB-to-Serial adapters
            RefreshComPortList();
        }

        private void RefreshComPortList()
        {
            // Determain if the list of com port names has changed since last checked
            string selected = RefreshComPortList(PoNamecBx.Items.Cast<string>(), PoNamecBx.SelectedItem as string, comport.IsOpen);

            // If there was an update, then update the control showing the user the list of port names
            if (!String.IsNullOrEmpty(selected))
            {
                PoNamecBx.Items.Clear();
                PoNamecBx.Items.AddRange(OrderedPortNames());
                PoNamecBx.SelectedItem = selected;
            }
        }

        private string[] OrderedPortNames()
        {
            // Just a placeholder for a successful parsing of a string to an integer
            int num;

            // Order the serial port names in numberic order (if possible)
            return SerialPort.GetPortNames().OrderBy(a => a.Length > 3 && int.TryParse(a.Substring(3), out num) ? num : 0).ToArray();
        }

        private string RefreshComPortList(IEnumerable<string> PreviousPortNames, string CurrentSelection, bool PortOpen)
        {
            // Create a new return report to populate
            string selected = null;

            // Retrieve the list of ports currently mounted by the operating system (sorted by name)
            string[] ports = SerialPort.GetPortNames();

            // First determain if there was a change (any additions or removals)
            bool updated = PreviousPortNames.Except(ports).Count() > 0 || ports.Except(PreviousPortNames).Count() > 0;

            // If there was a change, then select an appropriate default port
            if (updated)
            {
                // Use the correctly ordered set of port names
                ports = OrderedPortNames();

                // Find newest port if one or more were added
                string newest = SerialPort.GetPortNames().Except(PreviousPortNames).OrderBy(a => a).LastOrDefault();

                // If the port was already open... (see logic notes and reasoning in Notes.txt)
                if (PortOpen)
                {
                    if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else selected = ports.LastOrDefault();
                }
                else
                {
                    if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else selected = ports.LastOrDefault();
                }
            }

            // If there was a change to the port list, return the recommended default selection
            return selected;
        }

        #region AT command
        private void DiscovBtn_Click(object sender, EventArgs e)
        {
            prevIsAcq = false;
            IsDiscover = true;
            ChoosingDevice.DiscoveredDevicesList.Clear();
            SendData("ATDILE");
        }

        private void ConnecBtn_Click(object sender, EventArgs e)
        {
            if (myChoosingDevice == null)
            {
                myChoosingDevice = new ChoosingDevice();
            }

            myChoosingDevice.Show();
        }

        private void DataMoBtn_Click(object sender, EventArgs e)
        {
            SendData("ATMD");
        }

        private void CmdModBtn_Click(object sender, EventArgs e)
        {
            SendData("+++");
        }

        private void RCModeBtn_Click(object sender, EventArgs e)
        {
            SendData("ATMRC");
        }

        private void Reset_Btn_Click(object sender, EventArgs e)
        {
            SendData("ATRST");
        }

        private void ModuleBtn_Click(object sender, EventArgs e)
        {
            SendData("ATMT?");
        }

        private void StackTBtn_Click(object sender, EventArgs e)
        {
            SendData("ATST?");
        }

        private void SWVersBtn_Click(object sender, EventArgs e)
        {
            SendData("ATV?");
        }

        private void AddresBtn_Click(object sender, EventArgs e)
        {
            SendData("ATA?");
        }

        private void RSSI__Btn_Click(object sender, EventArgs e)
        {
            SendData("ATRSSI?,0");
        }

        private void DisconBtn_Click(object sender, EventArgs e)
        {
            SendData("+++");
            SendData("ATDH,0");
        }

        #endregion


        #region Oscilloscope
        // Oscilloscope

        private void eCGOscilloscopeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eCGOscilloscopeToolStripMenuItem.Checked = !(eCGOscilloscopeToolStripMenuItem.Checked);
            if ((eCGOscilloscopeToolStripMenuItem.Checked) && (myECGOscillo.Visible == false))
            {
                //ScriptToRun();                                                  // Re-generate all partial script to implement random effect
                myECGOscillo.Show();
            }
            else if (!(eCGOscilloscopeToolStripMenuItem.Checked) && (myECGOscillo.Visible == true))
            {
                myECGOscillo.Hide();
            }
        }

        // Event to monit the closing of SPListener object
        private void ECGOscillo_LogEvent(object sender, EventArgs e)
        {
            eCGOscilloscopeToolStripMenuItem.Checked = false;
        }

        #endregion

        private void StartABtn_Click(object sender, EventArgs e)
        {
            comport.DiscardInBuffer();
            tmpRemainedBytes.Clear();
            
            IsAcquire = true;
            prevIsAcq = true;
            
            myECGOscillo.Show();
            eCGOscilloscopeToolStripMenuItem.Checked = true;
            tmrCheckComPorts.Enabled = false;

            SendData("acquire");
        }

        private void StopABtn_Click(object sender, EventArgs e)
        {
            IsAcquire = false;
            SendData("stop_acq");
        }

        private void SaveABtn_Click(object sender, EventArgs e)
        {
            IsSaveClicked = true;
            SavingCounter = 0;
            //while (IsSaving)
            //{ }

        }

    }
}
