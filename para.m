

function Zp = para(Z1,Z2)

Zd = Z1+Z2;
Zn = Z1.*Z2;

Zp = Zn./Zd;

end