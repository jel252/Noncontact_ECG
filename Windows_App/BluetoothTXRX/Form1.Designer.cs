﻿namespace BluetoothTXRX
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportECGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eCGOscilloscopeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aTCommandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SaveABtn = new System.Windows.Forms.Button();
            this.StopABtn = new System.Windows.Forms.Button();
            this.StartABtn = new System.Windows.Forms.Button();
            this.OpenPoBtn = new System.Windows.Forms.Button();
            this.SendDCgBx = new System.Windows.Forms.GroupBox();
            this.SendDCBtn = new System.Windows.Forms.Button();
            this.DatCmdtBx = new System.Windows.Forms.TextBox();
            this.ATcmdsgBx = new System.Windows.Forms.GroupBox();
            this.DisconBtn = new System.Windows.Forms.Button();
            this.RSSI__Btn = new System.Windows.Forms.Button();
            this.AddresBtn = new System.Windows.Forms.Button();
            this.SWVersBtn = new System.Windows.Forms.Button();
            this.StackTBtn = new System.Windows.Forms.Button();
            this.ModuleBtn = new System.Windows.Forms.Button();
            this.Reset_Btn = new System.Windows.Forms.Button();
            this.RCModeBtn = new System.Windows.Forms.Button();
            this.CmdModBtn = new System.Windows.Forms.Button();
            this.DataMoBtn = new System.Windows.Forms.Button();
            this.ConnecBtn = new System.Windows.Forms.Button();
            this.DiscovBtn = new System.Windows.Forms.Button();
            this.PortSegBx = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FloCtlcBx = new System.Windows.Forms.ComboBox();
            this.StoBitcBx = new System.Windows.Forms.ComboBox();
            this.DatBitcBx = new System.Windows.Forms.ComboBox();
            this.ParitycBx = new System.Windows.Forms.ComboBox();
            this.BaRatecBx = new System.Windows.Forms.ComboBox();
            this.PoNamecBx = new System.Windows.Forms.ComboBox();
            this.LogHisDGV = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tmrCheckComPorts = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SendDCgBx.SuspendLayout();
            this.ATcmdsgBx.SuspendLayout();
            this.PortSegBx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogHisDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(577, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportLogToolStripMenuItem,
            this.exportECGToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exportLogToolStripMenuItem
            // 
            this.exportLogToolStripMenuItem.Name = "exportLogToolStripMenuItem";
            this.exportLogToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exportLogToolStripMenuItem.Text = "Export Log";
            // 
            // exportECGToolStripMenuItem
            // 
            this.exportECGToolStripMenuItem.Name = "exportECGToolStripMenuItem";
            this.exportECGToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exportECGToolStripMenuItem.Text = "Export ECG";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eCGOscilloscopeToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // eCGOscilloscopeToolStripMenuItem
            // 
            this.eCGOscilloscopeToolStripMenuItem.Name = "eCGOscilloscopeToolStripMenuItem";
            this.eCGOscilloscopeToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.eCGOscilloscopeToolStripMenuItem.Text = "ECG Oscilloscope";
            this.eCGOscilloscopeToolStripMenuItem.Click += new System.EventHandler(this.eCGOscilloscopeToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aTCommandToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aTCommandToolStripMenuItem
            // 
            this.aTCommandToolStripMenuItem.Name = "aTCommandToolStripMenuItem";
            this.aTCommandToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.aTCommandToolStripMenuItem.Text = "AT Command";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 633);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(577, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.OpenPoBtn);
            this.splitContainer1.Panel1.Controls.Add(this.SendDCgBx);
            this.splitContainer1.Panel1.Controls.Add(this.ATcmdsgBx);
            this.splitContainer1.Panel1.Controls.Add(this.PortSegBx);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LogHisDGV);
            this.splitContainer1.Size = new System.Drawing.Size(577, 609);
            this.splitContainer1.SplitterDistance = 191;
            this.splitContainer1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.SaveABtn);
            this.groupBox1.Controls.Add(this.StopABtn);
            this.groupBox1.Controls.Add(this.StartABtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 505);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 101);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ECG Acquire";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(17, 21);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(102, 16);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Arduino TI BLE";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // SaveABtn
            // 
            this.SaveABtn.Location = new System.Drawing.Point(87, 72);
            this.SaveABtn.Name = "SaveABtn";
            this.SaveABtn.Size = new System.Drawing.Size(75, 23);
            this.SaveABtn.TabIndex = 3;
            this.SaveABtn.Text = "Save";
            this.SaveABtn.UseVisualStyleBackColor = true;
            this.SaveABtn.Click += new System.EventHandler(this.SaveABtn_Click);
            // 
            // StopABtn
            // 
            this.StopABtn.Location = new System.Drawing.Point(87, 47);
            this.StopABtn.Name = "StopABtn";
            this.StopABtn.Size = new System.Drawing.Size(75, 23);
            this.StopABtn.TabIndex = 2;
            this.StopABtn.Text = "Stop";
            this.StopABtn.UseVisualStyleBackColor = true;
            this.StopABtn.Click += new System.EventHandler(this.StopABtn_Click);
            // 
            // StartABtn
            // 
            this.StartABtn.Location = new System.Drawing.Point(6, 47);
            this.StartABtn.Name = "StartABtn";
            this.StartABtn.Size = new System.Drawing.Size(75, 23);
            this.StartABtn.TabIndex = 1;
            this.StartABtn.Text = "Start";
            this.StartABtn.UseVisualStyleBackColor = true;
            this.StartABtn.Click += new System.EventHandler(this.StartABtn_Click);
            // 
            // OpenPoBtn
            // 
            this.OpenPoBtn.Location = new System.Drawing.Point(29, 189);
            this.OpenPoBtn.Name = "OpenPoBtn";
            this.OpenPoBtn.Size = new System.Drawing.Size(136, 23);
            this.OpenPoBtn.TabIndex = 3;
            this.OpenPoBtn.Text = "Open Port";
            this.OpenPoBtn.UseVisualStyleBackColor = true;
            this.OpenPoBtn.Click += new System.EventHandler(this.OpenPoBtn_Click);
            // 
            // SendDCgBx
            // 
            this.SendDCgBx.Controls.Add(this.SendDCBtn);
            this.SendDCgBx.Controls.Add(this.DatCmdtBx);
            this.SendDCgBx.Location = new System.Drawing.Point(12, 430);
            this.SendDCgBx.Name = "SendDCgBx";
            this.SendDCgBx.Size = new System.Drawing.Size(175, 77);
            this.SendDCgBx.TabIndex = 2;
            this.SendDCgBx.TabStop = false;
            this.SendDCgBx.Text = "Data / Command";
            // 
            // SendDCBtn
            // 
            this.SendDCBtn.Location = new System.Drawing.Point(48, 46);
            this.SendDCBtn.Name = "SendDCBtn";
            this.SendDCBtn.Size = new System.Drawing.Size(75, 23);
            this.SendDCBtn.TabIndex = 1;
            this.SendDCBtn.Text = "Send";
            this.SendDCBtn.UseVisualStyleBackColor = true;
            this.SendDCBtn.Click += new System.EventHandler(this.btnSend_Click);
            this.SendDCBtn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SendDCBtn_KeyPress);
            // 
            // DatCmdtBx
            // 
            this.DatCmdtBx.Location = new System.Drawing.Point(7, 21);
            this.DatCmdtBx.Name = "DatCmdtBx";
            this.DatCmdtBx.Size = new System.Drawing.Size(162, 22);
            this.DatCmdtBx.TabIndex = 0;
            // 
            // ATcmdsgBx
            // 
            this.ATcmdsgBx.Controls.Add(this.DisconBtn);
            this.ATcmdsgBx.Controls.Add(this.RSSI__Btn);
            this.ATcmdsgBx.Controls.Add(this.AddresBtn);
            this.ATcmdsgBx.Controls.Add(this.SWVersBtn);
            this.ATcmdsgBx.Controls.Add(this.StackTBtn);
            this.ATcmdsgBx.Controls.Add(this.ModuleBtn);
            this.ATcmdsgBx.Controls.Add(this.Reset_Btn);
            this.ATcmdsgBx.Controls.Add(this.RCModeBtn);
            this.ATcmdsgBx.Controls.Add(this.CmdModBtn);
            this.ATcmdsgBx.Controls.Add(this.DataMoBtn);
            this.ATcmdsgBx.Controls.Add(this.ConnecBtn);
            this.ATcmdsgBx.Controls.Add(this.DiscovBtn);
            this.ATcmdsgBx.Location = new System.Drawing.Point(12, 227);
            this.ATcmdsgBx.Name = "ATcmdsgBx";
            this.ATcmdsgBx.Size = new System.Drawing.Size(175, 196);
            this.ATcmdsgBx.TabIndex = 1;
            this.ATcmdsgBx.TabStop = false;
            this.ATcmdsgBx.Text = "AT Command";
            // 
            // DisconBtn
            // 
            this.DisconBtn.Location = new System.Drawing.Point(87, 166);
            this.DisconBtn.Name = "DisconBtn";
            this.DisconBtn.Size = new System.Drawing.Size(75, 23);
            this.DisconBtn.TabIndex = 11;
            this.DisconBtn.Text = "Disconnect";
            this.DisconBtn.UseVisualStyleBackColor = true;
            this.DisconBtn.Click += new System.EventHandler(this.DisconBtn_Click);
            // 
            // RSSI__Btn
            // 
            this.RSSI__Btn.Location = new System.Drawing.Point(6, 166);
            this.RSSI__Btn.Name = "RSSI__Btn";
            this.RSSI__Btn.Size = new System.Drawing.Size(75, 23);
            this.RSSI__Btn.TabIndex = 10;
            this.RSSI__Btn.Text = "RSSI";
            this.RSSI__Btn.UseVisualStyleBackColor = true;
            this.RSSI__Btn.Click += new System.EventHandler(this.RSSI__Btn_Click);
            // 
            // AddresBtn
            // 
            this.AddresBtn.Location = new System.Drawing.Point(87, 137);
            this.AddresBtn.Name = "AddresBtn";
            this.AddresBtn.Size = new System.Drawing.Size(75, 23);
            this.AddresBtn.TabIndex = 9;
            this.AddresBtn.Text = "Address";
            this.AddresBtn.UseVisualStyleBackColor = true;
            this.AddresBtn.Click += new System.EventHandler(this.AddresBtn_Click);
            // 
            // SWVersBtn
            // 
            this.SWVersBtn.Location = new System.Drawing.Point(6, 137);
            this.SWVersBtn.Name = "SWVersBtn";
            this.SWVersBtn.Size = new System.Drawing.Size(75, 23);
            this.SWVersBtn.TabIndex = 8;
            this.SWVersBtn.Text = "SW Version";
            this.SWVersBtn.UseVisualStyleBackColor = true;
            this.SWVersBtn.Click += new System.EventHandler(this.SWVersBtn_Click);
            // 
            // StackTBtn
            // 
            this.StackTBtn.Location = new System.Drawing.Point(87, 108);
            this.StackTBtn.Name = "StackTBtn";
            this.StackTBtn.Size = new System.Drawing.Size(75, 23);
            this.StackTBtn.TabIndex = 7;
            this.StackTBtn.Text = "Stack Type";
            this.StackTBtn.UseVisualStyleBackColor = true;
            this.StackTBtn.Click += new System.EventHandler(this.StackTBtn_Click);
            // 
            // ModuleBtn
            // 
            this.ModuleBtn.Location = new System.Drawing.Point(6, 108);
            this.ModuleBtn.Name = "ModuleBtn";
            this.ModuleBtn.Size = new System.Drawing.Size(75, 23);
            this.ModuleBtn.TabIndex = 6;
            this.ModuleBtn.Text = "Module Typ";
            this.ModuleBtn.UseVisualStyleBackColor = true;
            this.ModuleBtn.Click += new System.EventHandler(this.ModuleBtn_Click);
            // 
            // Reset_Btn
            // 
            this.Reset_Btn.Location = new System.Drawing.Point(87, 79);
            this.Reset_Btn.Name = "Reset_Btn";
            this.Reset_Btn.Size = new System.Drawing.Size(75, 23);
            this.Reset_Btn.TabIndex = 5;
            this.Reset_Btn.Text = "Reset";
            this.Reset_Btn.UseVisualStyleBackColor = true;
            this.Reset_Btn.Click += new System.EventHandler(this.Reset_Btn_Click);
            // 
            // RCModeBtn
            // 
            this.RCModeBtn.Location = new System.Drawing.Point(6, 79);
            this.RCModeBtn.Name = "RCModeBtn";
            this.RCModeBtn.Size = new System.Drawing.Size(75, 23);
            this.RCModeBtn.TabIndex = 4;
            this.RCModeBtn.Text = "RC Mode";
            this.RCModeBtn.UseVisualStyleBackColor = true;
            this.RCModeBtn.Click += new System.EventHandler(this.RCModeBtn_Click);
            // 
            // CmdModBtn
            // 
            this.CmdModBtn.Location = new System.Drawing.Point(87, 50);
            this.CmdModBtn.Name = "CmdModBtn";
            this.CmdModBtn.Size = new System.Drawing.Size(75, 23);
            this.CmdModBtn.TabIndex = 3;
            this.CmdModBtn.Text = "Cmd Mode";
            this.CmdModBtn.UseVisualStyleBackColor = true;
            this.CmdModBtn.Click += new System.EventHandler(this.CmdModBtn_Click);
            // 
            // DataMoBtn
            // 
            this.DataMoBtn.Location = new System.Drawing.Point(6, 50);
            this.DataMoBtn.Name = "DataMoBtn";
            this.DataMoBtn.Size = new System.Drawing.Size(75, 23);
            this.DataMoBtn.TabIndex = 2;
            this.DataMoBtn.Text = "Data Mode";
            this.DataMoBtn.UseVisualStyleBackColor = true;
            this.DataMoBtn.Click += new System.EventHandler(this.DataMoBtn_Click);
            // 
            // ConnecBtn
            // 
            this.ConnecBtn.Location = new System.Drawing.Point(87, 21);
            this.ConnecBtn.Name = "ConnecBtn";
            this.ConnecBtn.Size = new System.Drawing.Size(75, 23);
            this.ConnecBtn.TabIndex = 1;
            this.ConnecBtn.Text = "Connect";
            this.ConnecBtn.UseVisualStyleBackColor = true;
            this.ConnecBtn.Click += new System.EventHandler(this.ConnecBtn_Click);
            // 
            // DiscovBtn
            // 
            this.DiscovBtn.Location = new System.Drawing.Point(6, 21);
            this.DiscovBtn.Name = "DiscovBtn";
            this.DiscovBtn.Size = new System.Drawing.Size(75, 23);
            this.DiscovBtn.TabIndex = 0;
            this.DiscovBtn.Text = "Discover";
            this.DiscovBtn.UseVisualStyleBackColor = true;
            this.DiscovBtn.Click += new System.EventHandler(this.DiscovBtn_Click);
            // 
            // PortSegBx
            // 
            this.PortSegBx.Controls.Add(this.label6);
            this.PortSegBx.Controls.Add(this.label5);
            this.PortSegBx.Controls.Add(this.label4);
            this.PortSegBx.Controls.Add(this.label3);
            this.PortSegBx.Controls.Add(this.label2);
            this.PortSegBx.Controls.Add(this.label1);
            this.PortSegBx.Controls.Add(this.FloCtlcBx);
            this.PortSegBx.Controls.Add(this.StoBitcBx);
            this.PortSegBx.Controls.Add(this.DatBitcBx);
            this.PortSegBx.Controls.Add(this.ParitycBx);
            this.PortSegBx.Controls.Add(this.BaRatecBx);
            this.PortSegBx.Controls.Add(this.PoNamecBx);
            this.PortSegBx.Location = new System.Drawing.Point(12, 3);
            this.PortSegBx.Name = "PortSegBx";
            this.PortSegBx.Size = new System.Drawing.Size(175, 180);
            this.PortSegBx.TabIndex = 0;
            this.PortSegBx.TabStop = false;
            this.PortSegBx.Text = "Serial Com";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "Flow Ctrl:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "Stop Bit:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "Data Bit:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "Parity:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "Baudrate:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "Port:";
            // 
            // FloCtlcBx
            // 
            this.FloCtlcBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FloCtlcBx.FormattingEnabled = true;
            this.FloCtlcBx.Location = new System.Drawing.Point(74, 152);
            this.FloCtlcBx.Name = "FloCtlcBx";
            this.FloCtlcBx.Size = new System.Drawing.Size(95, 20);
            this.FloCtlcBx.TabIndex = 5;
            // 
            // StoBitcBx
            // 
            this.StoBitcBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StoBitcBx.FormattingEnabled = true;
            this.StoBitcBx.Location = new System.Drawing.Point(74, 126);
            this.StoBitcBx.Name = "StoBitcBx";
            this.StoBitcBx.Size = new System.Drawing.Size(95, 20);
            this.StoBitcBx.TabIndex = 4;
            // 
            // DatBitcBx
            // 
            this.DatBitcBx.FormattingEnabled = true;
            this.DatBitcBx.Location = new System.Drawing.Point(74, 100);
            this.DatBitcBx.Name = "DatBitcBx";
            this.DatBitcBx.Size = new System.Drawing.Size(95, 20);
            this.DatBitcBx.TabIndex = 3;
            // 
            // ParitycBx
            // 
            this.ParitycBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ParitycBx.FormattingEnabled = true;
            this.ParitycBx.Location = new System.Drawing.Point(74, 74);
            this.ParitycBx.Name = "ParitycBx";
            this.ParitycBx.Size = new System.Drawing.Size(95, 20);
            this.ParitycBx.TabIndex = 2;
            // 
            // BaRatecBx
            // 
            this.BaRatecBx.FormattingEnabled = true;
            this.BaRatecBx.Location = new System.Drawing.Point(74, 48);
            this.BaRatecBx.Name = "BaRatecBx";
            this.BaRatecBx.Size = new System.Drawing.Size(95, 20);
            this.BaRatecBx.TabIndex = 1;
            // 
            // PoNamecBx
            // 
            this.PoNamecBx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PoNamecBx.FormattingEnabled = true;
            this.PoNamecBx.Location = new System.Drawing.Point(74, 22);
            this.PoNamecBx.Name = "PoNamecBx";
            this.PoNamecBx.Size = new System.Drawing.Size(95, 20);
            this.PoNamecBx.TabIndex = 0;
            // 
            // LogHisDGV
            // 
            this.LogHisDGV.AllowUserToAddRows = false;
            this.LogHisDGV.AllowUserToDeleteRows = false;
            this.LogHisDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LogHisDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.LogHisDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogHisDGV.Location = new System.Drawing.Point(0, 0);
            this.LogHisDGV.Name = "LogHisDGV";
            this.LogHisDGV.ReadOnly = true;
            this.LogHisDGV.RowTemplate.Height = 24;
            this.LogHisDGV.Size = new System.Drawing.Size(382, 609);
            this.LogHisDGV.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "Time";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 54;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Content";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 67;
            // 
            // tmrCheckComPorts
            // 
            this.tmrCheckComPorts.Interval = 500;
            this.tmrCheckComPorts.Tick += new System.EventHandler(this.tmrCheckComPorts_Tick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "csv";
            this.saveFileDialog1.Filter = "逗點分隔檔(*.csv)|*.csv";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 655);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.SendDCgBx.ResumeLayout(false);
            this.SendDCgBx.PerformLayout();
            this.ATcmdsgBx.ResumeLayout(false);
            this.PortSegBx.ResumeLayout(false);
            this.PortSegBx.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogHisDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox ATcmdsgBx;
        private System.Windows.Forms.Button DisconBtn;
        private System.Windows.Forms.Button RSSI__Btn;
        private System.Windows.Forms.Button AddresBtn;
        private System.Windows.Forms.Button SWVersBtn;
        private System.Windows.Forms.Button StackTBtn;
        private System.Windows.Forms.Button ModuleBtn;
        private System.Windows.Forms.Button Reset_Btn;
        private System.Windows.Forms.Button RCModeBtn;
        private System.Windows.Forms.Button CmdModBtn;
        private System.Windows.Forms.Button DataMoBtn;
        private System.Windows.Forms.Button ConnecBtn;
        private System.Windows.Forms.Button DiscovBtn;
        private System.Windows.Forms.GroupBox PortSegBx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox FloCtlcBx;
        private System.Windows.Forms.ComboBox StoBitcBx;
        private System.Windows.Forms.ComboBox DatBitcBx;
        private System.Windows.Forms.ComboBox ParitycBx;
        private System.Windows.Forms.ComboBox BaRatecBx;
        private System.Windows.Forms.ComboBox PoNamecBx;
        private System.Windows.Forms.DataGridView LogHisDGV;
        private System.Windows.Forms.GroupBox SendDCgBx;
        private System.Windows.Forms.Button SendDCBtn;
        private System.Windows.Forms.TextBox DatCmdtBx;
        private System.Windows.Forms.ToolStripMenuItem exportLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportECGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aTCommandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button OpenPoBtn;
        private System.Windows.Forms.Timer tmrCheckComPorts;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button StopABtn;
        private System.Windows.Forms.Button StartABtn;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eCGOscilloscopeToolStripMenuItem;
        private System.Windows.Forms.Button SaveABtn;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

