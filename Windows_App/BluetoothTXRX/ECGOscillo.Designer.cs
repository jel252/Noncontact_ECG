﻿namespace BluetoothTXRX
{
    partial class ECGOscillo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.ECGCht = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.ECGCht)).BeginInit();
            this.SuspendLayout();
            // 
            // ECGCht
            // 
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.AxisY.Interval = 0.5D;
            chartArea1.AxisY.MajorGrid.Interval = 0.5D;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.AxisY.MajorTickMark.Interval = 1D;
            chartArea1.AxisY.Maximum = 2.5D;
            chartArea1.AxisY.Minimum = -2.5D;
            chartArea1.Name = "ChartArea1";
            this.ECGCht.ChartAreas.Add(chartArea1);
            this.ECGCht.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.AutoFitMinFontSize = 6;
            legend1.BackColor = System.Drawing.Color.Transparent;
            legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend1.Name = "Legend1";
            legend1.Position.Auto = false;
            legend1.Position.Height = 6.299212F;
            legend1.Position.Width = 27.33246F;
            legend1.Position.X = 69.66754F;
            legend1.Position.Y = 3F;
            this.ECGCht.Legends.Add(legend1);
            this.ECGCht.Location = new System.Drawing.Point(0, 0);
            this.ECGCht.Name = "ECGCht";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            series1.Legend = "Legend1";
            series1.Name = "Cont";
            this.ECGCht.Series.Add(series1);
            this.ECGCht.Size = new System.Drawing.Size(842, 382);
            this.ECGCht.TabIndex = 0;
            this.ECGCht.Text = "chart1";
            this.ECGCht.Click += new System.EventHandler(this.ECGCht_Click);
            // 
            // ECGOscillo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 382);
            this.Controls.Add(this.ECGCht);
            this.Name = "ECGOscillo";
            this.Text = "ECGOscillo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ECGOscillo_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.ECGOscillo_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.ECGCht)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataVisualization.Charting.Chart ECGCht;
    }
}